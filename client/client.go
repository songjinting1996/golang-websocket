/*
 * @Author: your name
 * @Date: 2021-04-06 10:48:24
 * @LastEditTime: 2021-04-06 17:49:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /songjintinglinux/go/src/golang_pratice/websocket/client/client.go
 */
package main

import (
	"fmt"
	"log"

	"golang.org/x/net/websocket"
)

var origin = "http://127.0.0.1:8080/"
var url = "ws://127.0.0.1:8080/echo"

func main() {
	//客户端与服务器建立连接
	ws, err := websocket.Dial(url, "", origin)
	if err != nil {
		log.Fatal(err)
	}

	//发送数据给服务器
	message := []byte("hello, world!你好")
	_, err = ws.Write(message)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Send: %s\n", message)

	//从服务器接收数据
	var msg = make([]byte, 512)
	m, err := ws.Read(msg)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Receive: %s\n", msg[:m])

	//关闭连接
	ws.Close()
}
