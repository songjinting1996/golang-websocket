/*
 * @Author: your name
 * @Date: 2021-04-06 10:48:17
 * @LastEditTime: 2021-04-06 17:45:14
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /songjintinglinux/go/src/golang_pratice/websocket/server/server.go
 */
package main

import (
	"fmt"
	"log"
	"net/http"

	"golang.org/x/net/websocket"
)

func echoHandler(ws *websocket.Conn) {
	msg := make([]byte, 512)

	//读取客户端发来的数据
	n, err := ws.Read(msg)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Receive: %s\n", msg[:n])

	//发送数据给客户端
	sendmsg := "[" + string(msg[:n]) + "]"
	m, err := ws.Write([]byte(sendmsg))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Send: %s\n", msg[:m])
}

func main() {
	//注册回调函数
	http.Handle("/echo", websocket.Handler(echoHandler))
	// http.Handle("/", http.FileServer(http.Dir(".")))

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
